package com.adsi.inventory.web.rest;

import com.adsi.inventory.service.UserService;
import com.adsi.inventory.service.dto.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class UserResource {

    @Autowired
    private UserService usersService;

    @GetMapping("/users")
    public ResponseEntity<List<UserDTO>> getAll() {
        return new ResponseEntity<>(usersService.getAll(),HttpStatus.OK);
    }

    @PostMapping("/users")
    public ResponseEntity<?> save(@RequestBody UserDTO userDTO){
        UserDTO dto = null;
        Map<String, Object> response =  new HashMap<>();
        try {
            dto = usersService.save(userDTO);
        }catch (DataAccessException err){
            response.put("Mensage: ", "Error al guardar este usuario");
            response.put("Error: ", err.getMessage() + ": " + err.getMostSpecificCause().getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }

        response.put("Mensage: ", "El usuario se ha creado sasticfactoriamente");
        response.put("usuario: ", dto);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @PutMapping("/users")
    public ResponseEntity<UserDTO> update(@RequestBody UserDTO userDTO){
        return new ResponseEntity<>(usersService.update(userDTO),HttpStatus.OK);
    }

    @GetMapping("/users/{id}")
    public UserDTO getById (@PathVariable Long id){
        return usersService.getById(id);
    }
}
