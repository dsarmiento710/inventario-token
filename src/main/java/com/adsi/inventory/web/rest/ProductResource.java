package com.adsi.inventory.web.rest;

import com.adsi.inventory.service.ProductService;
import com.adsi.inventory.service.dto.ProductDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class ProductResource {

    @Autowired
    ProductService service;

    @PostMapping("/product")
    public ResponseEntity<ProductDTO> create(@RequestBody ProductDTO productDTO){
        return new ResponseEntity<>(service.save(productDTO), HttpStatus.CREATED);
    }

    @GetMapping("/product")
    public ResponseEntity<List<ProductDTO>> get(){
        return ResponseEntity.status(200).body(service.getAll());
    }

    @GetMapping("/product/{reference}")
    public ResponseEntity<ProductDTO> getByReference(@PathVariable String reference){
        return new ResponseEntity<>(service.getByReference(reference), HttpStatus.OK);
    }
}
