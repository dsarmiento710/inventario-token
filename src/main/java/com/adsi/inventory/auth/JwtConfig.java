package com.adsi.inventory.auth;

public class JwtConfig {

    public static final String RSA_PRIVADA = "-----BEGIN RSA PRIVATE KEY-----\n" +
            "MIIEpAIBAAKCAQEAxngUlQOfOPMPMH7eF2ettZ89fo3qNzxibTD6LChfCit9L6SK\n" +
            "iOSt9tVUk+p1+mEyDDIEk55r/GsXjMhkMCxadzwLJlInd0J28726bwr/Banv75T3\n" +
            "8taw5Dj4nBZ1TPd7dvqeaqhaSxrXT6Fn9jZcPjkUtWpT1WAeqEto1ZFmmGIHp0iW\n" +
            "pEAK/6WGTB7JIhKAPUZUKGu/O0iKb/y4V1ALxLZStq6DCcDBZ01+juCcDqGZCAVE\n" +
            "zlAQplyVHTWfYWwGq68ImJMxd4s4EcZbiiJCCQipPU9fB/CeQa2hupkfw+mzFah/\n" +
            "DxY2LA3TcG9z1Gvcuufkjp26QoRyeOwDgqJm9QIDAQABAoIBAG2y8a1r0xYnQr4L\n" +
            "YbaOF/XLjrHkvxDIz56K87x0lBOK9ibfS7Nessk8IQOhFJTQJFmZS+5MImPizaGD\n" +
            "Id1+poJBLeiAgE/q7NTMib/W1B7NT1GdhW80O5Hp64Ba6XshT5zz6ZwKcd4Rd/EL\n" +
            "xerRIStDYa7klnHaLxfWlpt1Qr/2dgbpM1fVeLRFufLBhJihRaDcpmVZKePjLT1y\n" +
            "ezOtpa+ryZv/sUDCyjY7mtZnkEr83PQIed0MgrYIaTwqKCEO5KAZ/pTicAwxMLTC\n" +
            "dCqvMav1gXt2/YZT26cqCONMqJBy82jS+Jz1/TXlDklaFlS6XYh5z0CJGsvVT+oQ\n" +
            "+7QP4gECgYEA5nCo3Gkyqc7CR8sfrnDXhDYZKVKzl5OTjFJQZ1+ZwKGa92+FhRt5\n" +
            "1Etb0GrITas4lVeiIcJobHJTlqGz4j1b0lQo1qxELvEdvLp/vQ3x3ku8ur3/5tBX\n" +
            "VJaRC/4XedEqdv/V4lrdQvz5JMdNyvlKImcLRGHQXLYl/0C+2onL9nMCgYEA3Hua\n" +
            "yvVWFSLgp/EyuisVOuHYiVnLCy5mLAUGYBN3PrfJ/Zj1yenWrdY/zpQ/Ye6f6ZAC\n" +
            "eSuFRMArJczIfjE+TG8Nqs6tDYDD5HU32znnXpKA+0SQpl7TZNdlhHRwYIqGN9F7\n" +
            "FSDlithzLsj1kBniDRPsSdOHEPZRlvSNO3sEavcCgYEApJQVpcVkL67BRrEloVpq\n" +
            "MbzJX5/3L+xwVql7TzkSAGfq24lEnPowmpRvsHxzLjV5Jf/+hYnpNnq4hD5c0Nnt\n" +
            "uFvvBtLlp3kBOgcgsA9DjPYehmioWVq25GzcjuocLh5a675wLUej+vHdpozJXIuK\n" +
            "jON3QpFxVKWqoZDGPiJoD00CgYEAx00wEnCM3NRHMWhVS3XwXeaTHy083iGWGAp+\n" +
            "xdAyxU3LV+Kxo1AuT7tQuELHvJXAlZcotTFC6W+wUR34yiFnSB5Gw3TJd2BDMztG\n" +
            "P3PrElDqKLfE7a7f9dLDIGVEw+/T/ZbtZJ70/3pDPDJJz3WmId41hFKiUBHolMUA\n" +
            "+Zhs2OcCgYB+C++pMJPqk54rz1+mzP6+ViHQbLYR2/m4G88zXB68wWiDv1ISG77a\n" +
            "3PaJHEE/0NoT69l7sO6OAEsdUD0RJCbbUl9M5AlFS7d2+JVpS0ASpFRLXdme5Mo5\n" +
            "0xsI31NbbRrVQK1WZ7/17T9hOcysruG38kKTQEhktORI0+kAX7kJHw==\n" +
            "-----END RSA PRIVATE KEY-----";

    public static final  String RSA_PUBLICA = "-----BEGIN PUBLIC KEY-----\n" +
            "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAxngUlQOfOPMPMH7eF2et\n" +
            "tZ89fo3qNzxibTD6LChfCit9L6SKiOSt9tVUk+p1+mEyDDIEk55r/GsXjMhkMCxa\n" +
            "dzwLJlInd0J28726bwr/Banv75T38taw5Dj4nBZ1TPd7dvqeaqhaSxrXT6Fn9jZc\n" +
            "PjkUtWpT1WAeqEto1ZFmmGIHp0iWpEAK/6WGTB7JIhKAPUZUKGu/O0iKb/y4V1AL\n" +
            "xLZStq6DCcDBZ01+juCcDqGZCAVEzlAQplyVHTWfYWwGq68ImJMxd4s4EcZbiiJC\n" +
            "CQipPU9fB/CeQa2hupkfw+mzFah/DxY2LA3TcG9z1Gvcuufkjp26QoRyeOwDgqJm\n" +
            "9QIDAQAB\n" +
            "-----END PUBLIC KEY-----";
}
