package com.adsi.inventory.service.imp;

import com.adsi.inventory.domain.Users;
import com.adsi.inventory.repository.UserRepository;
import com.adsi.inventory.service.UserService;
import com.adsi.inventory.service.dto.UserDTO;
import com.adsi.inventory.service.dto.UserTransformer;
import com.adsi.inventory.service.error.ObjectNotFoundExeption;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserServiceImp implements UserService {

    private Logger logger = LoggerFactory.getLogger(UserServiceImp.class);

    @Autowired
    private UserRepository userRepository;


    @Override
    public List<UserDTO> getAll() {
        return userRepository.findAll()
                .stream().map(UserTransformer::getUsersDTOFromUsers).collect(Collectors.toList());
    }

    @Override
    public UserDTO save(UserDTO userDTO) {
        return UserTransformer.getUsersDTOFromUsers(userRepository.save(UserTransformer.getUsersFromUsersDTO(userDTO)));
    }

    @Override
    public UserDTO update(UserDTO userDTO) {
        return UserTransformer.getUsersDTOFromUsers(userRepository.save(UserTransformer.getUsersFromUsersDTO(userDTO)));
    }

    @Override
    public UserDTO getById(Long id) {
        Optional<Users> user = userRepository.findById(id);

        if (!user.isPresent()) {
            throw new ObjectNotFoundExeption("El usuario con id" + id + "no fue encontrado");
        }
        return UserTransformer.getUsersDTOFromUsers(user.get());
    }

    @Override
    public UserDetails findByUserName(String userName) throws UsernameNotFoundException {

        Users users = userRepository.findByUsername(userName);

        if (users == null){
            logger.error("Error, username " + userName + " no encontrado.");
            throw new UsernameNotFoundException("Error, usarname " + userName + " no encontrado.");
        }

        List<GrantedAuthority> authorities = users.getRols()
                .stream()
                .map(role -> new SimpleGrantedAuthority(role.getName()))
                .peek(authority -> logger.info("ROLES: "+ authority.getAuthority()))
                .collect(Collectors.toList());
        return new User(users.getUsername(), users.getPassword(), users.getEnabled(),
                true, true, true, authorities);
    }
}
